use std::num;
use std::convert::From;

#[derive(Debug)]
pub enum NMEASentence {
    GGA(GGASentence),
    GST(GSTSentence),
    Unimplemented()
}

#[derive(Debug)]
pub struct GGASentence {
    pub time: Option<NMEATime>,
    pub latitude: Option<f64>,
    pub longitude: Option<f64>,
    pub quality: u8,
    pub num_satellites: u8,
    pub hdop: f32,
    pub altitude: Option<f64>,
    pub geo_separation: Option<f32>,
}

#[derive(Debug)]
pub struct GSTSentence {
    pub time: Option<NMEATime>,
    pub range_rms: f32,
    pub std_latitude: f32,
    pub std_longitude: f32,
    pub std_altitude: f32,
}

#[derive(Debug)]
pub struct NMEATime {
    millis: u32,
}

#[derive(Debug)]
pub enum NMEAError {
    ParseIntError(num::ParseIntError),
    ParseFloatError(num::ParseFloatError),
    UndefinedError(),
}

pub fn parse_nmea_sentence(nmea: &str) -> Result<NMEASentence, NMEAError> {
    let payload = nmea.split("*").next().ok_or(NMEAError::UndefinedError())?;
    //let (payload, cs) = nmea.rsplit_once("*").ok_or(NMEAError::UndefinedError())?;
    let nmea_split: Vec<&str> = payload.split(",").collect();

    let sentence = match nmea_split.get(0) {
        Some(&"$GNGGA") => {
            let time = parse_time(nmea_split.get(1).unwrap())?;
            let latitude = parse_lat(nmea_split.get(2).unwrap(), nmea_split.get(3).unwrap())?;
            let longitude = parse_lon(nmea_split.get(4).unwrap(), nmea_split.get(5).unwrap())?;
            let quality = nmea_split.get(6).unwrap().parse::<u8>()?;
            let num_satellites = nmea_split.get(7).unwrap().parse::<u8>()?;
            let hdop = nmea_split.get(8).unwrap().parse::<f32>()?;
            let altitude = parse_alt(nmea_split.get(9).unwrap())?;
            let geo_separation = parse_geo_sep(nmea_split.get(11).unwrap())?;
            NMEASentence::GGA(GGASentence {
                time, latitude, longitude, quality,
                num_satellites, hdop, altitude, geo_separation,
            })
        },
        Some(&"$GNGST") => {
            let time = parse_time(nmea_split.get(1).unwrap())?;
            let range_rms = nmea_split.get(2).unwrap().parse::<f32>()?;
            let std_latitude = nmea_split.get(6).unwrap().parse::<f32>()?;
            let std_longitude = nmea_split.get(7).unwrap().parse::<f32>()?;
            let std_altitude = nmea_split.get(8).unwrap().parse::<f32>()?;
            NMEASentence::GST(GSTSentence {
                time, range_rms, std_latitude, std_longitude, std_altitude,
            })
        },
        _ => NMEASentence::Unimplemented()
    };

    Ok(sentence)
}

fn parse_time(s: &str) -> Result<Option<NMEATime>,NMEAError> {
    if s.len() == 0 {
        return Ok(None);
    }
    if s.len() < 5 {
        return Err(NMEAError::UndefinedError());
    }
    let hh = s[..2].parse::<u32>()?;
    let mm = s[2..4].parse::<u32>()?;
    let ss = s[4..].parse::<f64>()?;

    let millis = (hh*3600000) + (mm*60000) + ((ss*1000.0) as u32);
    Ok(Some(NMEATime { millis }))
}

fn parse_lat(lat: &str, dir: &str) -> Result<Option<f64>,NMEAError> {
    if lat.len() == 0 {
        return Ok(None);
    }
    if lat.len() < 3 {
        return Err(NMEAError::UndefinedError());
    }
    let hh = lat[..2].parse::<f64>()?;
    let mm = lat[2..].parse::<f64>()?;
    let val = hh + (mm/60.0);

    if dir == "S" {
        Ok(Some(-val))
    } else {
        Ok(Some(val))
    }
}

fn parse_lon(lon: &str, dir: &str) -> Result<Option<f64>,NMEAError> {
    if lon.len() == 0 {
        return Ok(None);
    }
    if lon.len() < 4 {
        return Err(NMEAError::UndefinedError());
    }
    let hh = lon[..3].parse::<f64>()?;
    let mm = lon[3..].parse::<f64>()?;
    let val = hh + (mm/60.0);

    if dir == "W" {
        Ok(Some(-val))
    } else {
        Ok(Some(val))
    }
}

fn parse_alt(alt: &str) -> Result<Option<f64>,NMEAError> {
    if alt.len() == 0 {
        return Ok(None);
    }
    Ok(Some(alt.parse::<f64>()?))
}

fn parse_geo_sep(sep: &str) -> Result<Option<f32>,NMEAError> {
    if sep.len() == 0 {
        return Ok(None);
    }
    Ok(Some(sep.parse::<f32>()?))
}

impl From<num::ParseIntError> for NMEAError {
    fn from(err: num::ParseIntError) -> Self {
        NMEAError::ParseIntError(err)
    }
}

impl From<num::ParseFloatError> for NMEAError {
    fn from(err: num::ParseFloatError) -> Self {
        NMEAError::ParseFloatError(err)
    }
}
