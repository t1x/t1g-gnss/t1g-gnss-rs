use std::io;
use std::io::{BufReader, Read};
use std::net::TcpStream;
use std::thread;
use std::time::{Duration, Instant};

const GNSS_STREAM_ADDR: &str = "127.0.0.1:6789";
const RETRY_INTERVAL: Duration = Duration::from_secs(10);

pub struct PersistentReader {
    src_addr: String,
    reader: Option<TcpStream>,
    last_fail: Option<Instant>,
}

pub fn get_nmea_reader() -> BufReader<PersistentReader> {
    let inner_reader = PersistentReader::new(GNSS_STREAM_ADDR);
    BufReader::new(inner_reader)
}

impl PersistentReader {
    pub fn new(src_addr: &str) -> PersistentReader {
        let src_addr = String::from(src_addr);
        
        PersistentReader {
            src_addr,
            reader: None,
            last_fail: None,
        }
    }

    fn try_connect(&mut self) {
        if let Some(i) = self.last_fail {
            let elapsed = i.elapsed();
            if elapsed < RETRY_INTERVAL {
                thread::sleep(RETRY_INTERVAL - elapsed);
            }
        }
        if matches!(self.last_fail, None) {
            println!("Connecting to '{}'", &self.src_addr);
        }
        self.reader = match TcpStream::connect(&self.src_addr) {
            Ok(r) => {
                println!("Connected to '{}'", &self.src_addr);
                self.last_fail = None;
                Some(r)
            }
            Err(e) => {
                if matches!(self.last_fail, None) {
                    println!("Failed to connect to '{}': {}", &self.src_addr, e);
                    println!("Continuing to attempt connection to '{}'", &self.src_addr);
                }
                self.last_fail = Some(Instant::now());
                None
            }
        }
    }
}

impl Read for PersistentReader {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        loop {
            if matches!(self.reader, None) {
                self.try_connect();
            }
            if let Some(r) = &mut self.reader {
                match r.read(buf) {
                    Ok(s) if s == 0 => {
                        println!("Lost stream connection");
                        self.reader = None;
                    },
                    Ok(s) => return Ok(s),
                    Err(e) => {
                        println!("Stream read error: {}", e);
                        self.reader = None;
                    },
                }
            }
        }
    }
}
