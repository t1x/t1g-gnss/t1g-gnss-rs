use std::process::Command;
use std::sync::{Arc, Condvar, Mutex};
use std::thread;
use std::time::Duration;

use evdev::{Device, InputEventKind, Key};

fn main() {
    let cond_pair_tx = Arc::new((Mutex::new(false), Condvar::new()));
    let cond_pair_rx = Arc::clone(&cond_pair_tx);

    // This thread tracks the state of the button and how long it has been held down. If
    // it reaches the critical time threshold, we trigger the 'poweroff' process.
    thread::spawn(move|| {
        let (lock, cvar) = &*cond_pair_rx;
        let mut pressed = lock.lock().unwrap();
        let mut timed_out;
        loop {
            if *pressed {
                let result = cvar.wait_timeout_while(pressed, Duration::from_secs(3), |p| { *p == true }).unwrap();
                pressed = result.0;
                timed_out = result.1;
                if timed_out.timed_out() {
                    println!("Shutting down");
                    Command::new("/sbin/poweroff").status().expect("poweroff failed");
                }
            }
            else {
                pressed = cvar.wait_while(pressed, |p| { *p == false }).unwrap();
            }
        }
    });

    // Synchronously wait for button events. Signal the button state to a separate thread for
    // evaluation of the length of the press and response action.
    let mut d = Device::open("/dev/input/by-path/platform-buttons-event").unwrap();
    let (lock, cvar) = &*cond_pair_tx;
    loop {
        for ev in d.fetch_events().unwrap() {
            if let InputEventKind::Key(k) = ev.kind() {
                if k == Key::BTN_0 {
                    let mut pressed = lock.lock().unwrap();
                    if ev.value() == 0 {
                        println!("Released");
                        *pressed = false;
                    }
                    if ev.value() == 1 {
                        println!("Pressed");
                        *pressed = true;
                    }
                    cvar.notify_all();
                }
            }
        }
    }
}