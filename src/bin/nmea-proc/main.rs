mod leds;

use std::fs;
use std::io::BufRead;
use std::path::{Path, PathBuf};
use std::time::{Duration, Instant};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use signal_hook::consts::{SIGTERM};
use serde::Serialize;

use t1g_gnss_rs::nmea;
use t1g_gnss_rs::nmea::NMEASentence;
use t1g_gnss_rs::nmea_stream;
use leds::{LEDContext, LEDState};

const GPS_DIR: &str = "/run/gnss-service";
const GPS_FILENAME: &str = "gps.json";
const GPS_BUF_FILENAME: &str = "gps.json.new";
const GPS_RAW_FILENAME: &str = "gps_raw.txt";
const GPS_RAW_BUF_FILENAME: &str = "gps_raw.txt.new";

#[derive(Debug,Serialize)]
struct GPSStateJson {
    lat: f64,
    lng: f64,
    alt: f64,
    lock: u8,
    accuracy: String,
}

fn main() {
    let gps_file: PathBuf = [GPS_DIR, GPS_FILENAME].iter().collect();
    let gps_file_buf: PathBuf = [GPS_DIR, GPS_BUF_FILENAME].iter().collect();
    let gps_raw_file: PathBuf = [GPS_DIR, GPS_RAW_FILENAME].iter().collect();
    let gps_raw_file_buf: PathBuf = [GPS_DIR, GPS_RAW_BUF_FILENAME].iter().collect();

    let need_term = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(SIGTERM, Arc::clone(&need_term)).unwrap();

    let update_interval = Duration::from_secs(1);
    let mut led_ctx = LEDContext::new();
    let mut last_update = Instant::now();
    let mut state = GPSStateJson {
        lat: 0.0, lng: 0.0, alt: 0.0, lock: 0, accuracy: "".to_string()
    };

    let mut str_reader = nmea_stream::get_nmea_reader();
    let mut nmea_cache = String::new();
    let mut nmea = String::new();
    loop {
        if need_term.load(Ordering::Relaxed) {
            println!("Received SIGTERM");
            led_ctx.set_state(&LEDState::off());
            break;
        }

        nmea.clear();
        if let Err(e) = str_reader.read_line(&mut nmea) {
            println!("Error reading NMEA: {}", e);
            continue;
        }

        nmea_cache.push_str(&nmea);

        match nmea::parse_nmea_sentence(&nmea) {
            Ok(n) => {
                match n {
                    NMEASentence::GGA(s) => {
                        state.lat = s.latitude.unwrap_or(0.0);
                        state.lng = s.longitude.unwrap_or(0.0);
                        state.alt = s.altitude.unwrap_or(0.0);
                        state.lock = s.quality;
                    },
                    NMEASentence::GST(s) => {
                        state.accuracy = format!("{},{},{}",s.std_latitude, s.std_longitude, s.std_altitude);
                    },
                    _ => (),
                };
            },
            Err(e) => println!("{:#?}", e),
        }

        if last_update.elapsed() > update_interval {
            last_update = Instant::now();

            // To guarantee an atomic update, write to a separate file and then move over the target
            fs::create_dir_all(Path::new(GPS_DIR)).unwrap();
            fs::write(&gps_file_buf, &serde_json::to_string(&state).unwrap()).unwrap();
            fs::rename(&gps_file_buf, &gps_file).unwrap();

            fs::write(&gps_raw_file_buf, &nmea_cache).unwrap();
            fs::rename(&gps_raw_file_buf, &gps_raw_file).unwrap();
            nmea_cache.clear();

            // Update the LED color
            match state.lock {
               1 =>  led_ctx.set_state(&LEDState::flash(&leds::RED, 1)),
               2 =>  led_ctx.set_state(&LEDState::flash(&leds::PURPLE, 1)),
               4 =>  led_ctx.set_state(&LEDState::solid(&leds::GREEN)),
               5 =>  led_ctx.set_state(&LEDState::flash(&leds::YELLOW, 1)),
               _ =>  led_ctx.set_state(&LEDState::flash(&leds::WHITE, 1)),
            };
        }
    }
}

