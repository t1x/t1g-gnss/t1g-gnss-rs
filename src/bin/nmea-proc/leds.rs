use std::fs;
use std::path::Path;
use std::time::{Duration, Instant};

const STATUS_LED_R_DIR: &str = "/sys/class/leds/red:status";
const STATUS_LED_G_DIR: &str = "/sys/class/leds/green:status";
const STATUS_LED_B_DIR: &str = "/sys/class/leds/blue:status";
const LED_TRIGGER_FILE: &str = "trigger";
const LED_BRIGHTNESS_FILE: &str = "brightness";
const LED_DELAY_ON_FILE: &str = "delay_on";
const LED_DELAY_OFF_FILE: &str = "delay_off";
const LED_DELAY_DEFAULT: u16 = 500;

const RETRIGGER_TIME: Duration = Duration::from_secs(60);

#[allow(dead_code)]
pub const RED: LEDMix    = LEDMix { r:227, g:0,  b:0 };
#[allow(dead_code)]
pub const GREEN: LEDMix  = LEDMix { r:0,   g:31, b:8 };
#[allow(dead_code)]
pub const BLUE: LEDMix   = LEDMix { r:0,   g:0,  b:255 };
#[allow(dead_code)]
pub const YELLOW: LEDMix = LEDMix { r:129, g:31, b:0 };
#[allow(dead_code)]
pub const PURPLE: LEDMix = LEDMix { r:227, g:0,  b:200 };
#[allow(dead_code)]
pub const ORANGE: LEDMix = LEDMix { r:227, g:26, b:0 };
#[allow(dead_code)]
pub const WHITE: LEDMix  = LEDMix { r:102, g:31, b:49 };
#[allow(dead_code)]
pub const BLACK: LEDMix  = LEDMix { r:0, g:0, b:0 };

#[derive(Copy, Clone, PartialEq)]
pub struct LEDMix {
    r: u8,
    g: u8,
    b: u8,
}

#[derive(Copy, Clone, PartialEq)]
pub struct LEDState {
    pub mix: LEDMix,
    pub on_millis: u16,
    pub off_millis: u16,
}

pub struct LEDContext {
    last_state: Option<LEDState>,
    last_change: Option<Instant>,
}

impl LEDState {
    pub fn off() -> LEDState {
        LEDState {
            mix: BLACK,
            on_millis: 0,
            off_millis: 0,
        }
    }

    pub fn solid(mix: &LEDMix) -> LEDState {
        LEDState {
            mix: *mix,
            on_millis: 1,
            off_millis: 0,
        }
    }

    pub fn flash(mix: &LEDMix, hz: u16) -> LEDState {
        let delay = match hz {
            0 => 0,
            _ => 500/hz,
        };
        LEDState {
            mix: *mix,
            on_millis: delay,
            off_millis: delay,
        }
    }
}

impl LEDContext {
    pub fn new() -> LEDContext {
        LEDContext {
            last_state: None,
            last_change: None,
        }
    }

    pub fn set_state(&mut self, new_state: &LEDState) {
        if let Some(ls) = self.last_state {
            if ls == *new_state {
                if let Some(i) = self.last_change {
                    if i.elapsed() < RETRIGGER_TIME {
                        return;
                    }
                }
            }
        }
        write_led_state(new_state);
        self.last_state = Some(*new_state);
        self.last_change = Some(Instant::now());
    }
}

fn write_led_state(s: &LEDState) {
    fs::write(Path::new(STATUS_LED_R_DIR).join(LED_BRIGHTNESS_FILE), "0").unwrap();
    fs::write(Path::new(STATUS_LED_G_DIR).join(LED_BRIGHTNESS_FILE), "0").unwrap();
    fs::write(Path::new(STATUS_LED_B_DIR).join(LED_BRIGHTNESS_FILE), "0").unwrap();

    if s.on_millis == 0 {
        return;
    }

    if s.off_millis == 0 {
        fs::write(Path::new(STATUS_LED_R_DIR).join(LED_BRIGHTNESS_FILE), s.mix.r.to_string()).unwrap();
        fs::write(Path::new(STATUS_LED_G_DIR).join(LED_BRIGHTNESS_FILE), s.mix.g.to_string()).unwrap();
        fs::write(Path::new(STATUS_LED_B_DIR).join(LED_BRIGHTNESS_FILE), s.mix.b.to_string()).unwrap();
    } else {
        fs::write(Path::new(STATUS_LED_R_DIR).join(LED_TRIGGER_FILE), "timer").unwrap();
        fs::write(Path::new(STATUS_LED_R_DIR).join(LED_BRIGHTNESS_FILE), s.mix.r.to_string()).unwrap();
        fs::write(Path::new(STATUS_LED_G_DIR).join(LED_TRIGGER_FILE), "timer").unwrap();
        fs::write(Path::new(STATUS_LED_G_DIR).join(LED_BRIGHTNESS_FILE), s.mix.g.to_string()).unwrap();
        fs::write(Path::new(STATUS_LED_B_DIR).join(LED_TRIGGER_FILE), "timer").unwrap();
        fs::write(Path::new(STATUS_LED_B_DIR).join(LED_BRIGHTNESS_FILE), s.mix.b.to_string()).unwrap();

        if s.on_millis != LED_DELAY_DEFAULT || s.off_millis != LED_DELAY_DEFAULT {
            fs::write(Path::new(STATUS_LED_R_DIR).join(LED_DELAY_ON_FILE), "0").unwrap();
            fs::write(Path::new(STATUS_LED_G_DIR).join(LED_DELAY_ON_FILE), "0").unwrap();
            fs::write(Path::new(STATUS_LED_B_DIR).join(LED_DELAY_ON_FILE), "0").unwrap();

            fs::write(Path::new(STATUS_LED_R_DIR).join(LED_DELAY_OFF_FILE), s.off_millis.to_string()).unwrap();
            fs::write(Path::new(STATUS_LED_G_DIR).join(LED_DELAY_OFF_FILE), s.off_millis.to_string()).unwrap();
            fs::write(Path::new(STATUS_LED_B_DIR).join(LED_DELAY_OFF_FILE), s.off_millis.to_string()).unwrap();

            fs::write(Path::new(STATUS_LED_R_DIR).join(LED_DELAY_ON_FILE), s.on_millis.to_string()).unwrap();
            fs::write(Path::new(STATUS_LED_G_DIR).join(LED_DELAY_ON_FILE), s.on_millis.to_string()).unwrap();
            fs::write(Path::new(STATUS_LED_B_DIR).join(LED_DELAY_ON_FILE), s.on_millis.to_string()).unwrap();
        }
    }
}
