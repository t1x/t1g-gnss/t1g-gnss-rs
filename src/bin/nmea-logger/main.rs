mod nmea_log;

use std::io::{Write, BufRead};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

use signal_hook::consts::{SIGHUP, SIGUSR1};
use t1g_gnss_rs::nmea_stream;
use nmea_log::NMEALogController;

fn main() {
    let mut logs = NMEALogController::new();
    logs.rotate();

    let need_reload = Arc::new(AtomicBool::new(false));
    let need_rotate = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(SIGHUP, Arc::clone(&need_reload)).unwrap();
    signal_hook::flag::register(SIGUSR1, Arc::clone(&need_rotate)).unwrap();

    let mut str_reader = nmea_stream::get_nmea_reader();
    let mut nmea = String::new();
    loop {
        if need_reload.load(Ordering::Relaxed) {
            need_reload.store(false,Ordering::Relaxed);
            println!("Received SIGHUP");
            logs.load_config();
        }
        if need_rotate.load(Ordering::Relaxed) {
            need_rotate.store(false,Ordering::Relaxed);
            println!("Received SIGUSR1");
            logs.rotate();
        }


        nmea.clear();
        match str_reader.read_line(&mut nmea) {
            Ok(_) => { logs.write(nmea.as_bytes()).unwrap(); },
            Err(e) => println!("Error reading NMEA: {}", e),
        }
    }
}

