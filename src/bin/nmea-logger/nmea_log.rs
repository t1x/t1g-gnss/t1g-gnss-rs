use std::cmp;
use std::fs;
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::os::unix::fs::FileTypeExt;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::time::{Instant, Duration};

use nix::mount::{mount, umount, MsFlags};
use nix::sys::statvfs;
use serde::Deserialize;
use chrono::{Datelike, DateTime, Timelike, Utc};

const LOCAL_LOG_DIR: &str = "/mnt/datafs/log/gnss";
const LOCAL_LOG_MOUNT_DIR: &str = "/mnt/datafs";
const LOCAL_LOG_DEV_PATH: &str = "/dev/mmcblk0p3";
const USB_LOG_DIR: &str = "/media/usb/log/gnss";
const USB_LOG_MOUNT_DIR: &str = "/media/usb";
const USB_LOG_DEV_PATH: &str = "/dev/sda1";
const SD_LOG_DIR: &str = "/media/sd/log/gnss";
const SD_LOG_MOUNT_DIR: &str = "/media/sd";
const SD_LOG_DEV_PATH: &str = "/dev/mmcblk1p1";

const LOG_FS_TYPES: &'static [&str] = &["ext4", "vfat"];

const CONF_DIR: &str = "/mnt/datafs/config/gnss-service";
const DEFAULT_CONF_DIR: &str = "/usr/share/gnss-service/config-defaults";
const LOG_CONF_FILENAME: &str = "log_settings.json";

const RETRY_INTERVAL: Duration = Duration::from_secs(10);
const MAX_LOG_DIR_SIZE: u64 = 1_000_000_000;
const MIN_LOG_DIR_SIZE: u64 = 10_000_000;
const MIN_LOG_FREE_SPACE: u64 = 100_000_000;

pub struct NMEALog {
    name: String,
    dev_path: PathBuf,
    mount_path: PathBuf,
    base_path: PathBuf,
    cur_log_path: Option<PathBuf>,
    is_enabled: bool,
    manage_mount: bool,
    writer: Option<BufWriter<File>>,
    start_time: Option<DateTime<Utc>>,
    failure_time: Option<Instant>,
}

pub struct NMEALogController {
    local_log: NMEALog,
    usb_log: NMEALog,
    sd_log: NMEALog,
}

#[derive(Debug,Deserialize)]
struct LogSettingsJson {
    #[serde(rename = "Log NMEA local")]
    log_local: bool,
    #[serde(rename = "Log NMEA USB")]
    log_usb: bool,
    #[serde(rename = "Log NMEA SD")]
    log_sd: bool,
}

impl NMEALogController {
    pub fn new() -> NMEALogController {
        let mut c = NMEALogController {
            local_log: NMEALog::new(
                "Local",
                Path::new(LOCAL_LOG_DEV_PATH),
                Path::new(LOCAL_LOG_MOUNT_DIR),
                Path::new(LOCAL_LOG_DIR),
                false),
            usb_log: NMEALog::new(
                "USB",
                Path::new(USB_LOG_DEV_PATH),
                Path::new(USB_LOG_MOUNT_DIR),
                Path::new(USB_LOG_DIR),
                false),
            sd_log: NMEALog::new(
                "SD",
                Path::new(SD_LOG_DEV_PATH),
                Path::new(SD_LOG_MOUNT_DIR),
                Path::new(SD_LOG_DIR),
                false),
        };
        c.load_config();
        c
    }

    pub fn load_config(&mut self) {
        let log_conf = match load_log_conf() {
            Some(c) => c,
            None => {
                println!("Could not load log configuration. Using sane default.");
                LogSettingsJson {
                    log_local: true,
                    log_usb: false,
                    log_sd: false,
                }
            },
        };

        println!("Bouncing any open log files");
        self.local_log.close_writer(false);
        self.usb_log.close_writer(false);
        self.sd_log.close_writer(false);

        self.local_log.set_enabled(log_conf.log_local);
        self.usb_log.set_enabled(log_conf.log_usb);
        self.sd_log.set_enabled(log_conf.log_sd);

        println!("Set log config (Local: {} / USB: {} / SD: {})", log_conf.log_local, log_conf.log_usb, log_conf.log_sd);
    }

    pub fn rotate(&mut self) {
        let _ = self.local_log.rotate();
        let _ = self.usb_log.rotate();
        let _ = self.sd_log.rotate();
    }
}

impl io::Write for NMEALogController {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let res = self.local_log.write(buf);
        let _ = self.usb_log.write(buf);
        let _ = self.sd_log.write(buf);
        res
    }

    fn flush(&mut self) -> io::Result<()> {
        let res = self.local_log.flush();
        let _ = self.usb_log.flush();
        let _ = self.sd_log.flush();
        res
    }
}

#[allow(dead_code)]
impl NMEALog {
    pub fn new(name: &str, dev_path: &Path, mount_path: &Path, base_path: &Path, manage_mount: bool) -> NMEALog {
        NMEALog {
            name: name.to_string(),
            dev_path: dev_path.to_path_buf(),
            mount_path: mount_path.to_path_buf(),
            base_path: base_path.to_path_buf(),
            cur_log_path: None,
            is_enabled: false,
            manage_mount,
            writer: None,
            start_time: None,
            failure_time: None,
        }
    }

    pub fn set_enabled(&mut self, enabled: bool) {
        if enabled && !self.is_enabled {
            println!("{}: Enabling NMEA log at '{}'", &self.name, self.base_path.to_str().unwrap());
            self.is_enabled = true;
            let _ = self.open_writer();
        }
        if !enabled && self.is_enabled {
            println!("{}: Disabling NMEA log at '{}'", &self.name, self.base_path.to_str().unwrap());
            self.is_enabled = false;
            self.close_writer(true);
        }
    }

    fn open_writer(&mut self) -> io::Result<()> {
        if self.writer.is_some() {
            self.close_writer(false);
        }

        if let Some(ft) = self.failure_time {
            if ft.elapsed() < RETRY_INTERVAL {
                return Err(io::Error::from(io::ErrorKind::NotFound));
            }
        }

        // There could technically be a small race if the device is unmounted in between this
        // check and any file operations. Not much to do about that?
        if !matches!(self.try_mount(),Ok(())) {
            //println!("'{}' is not mounted", self.mount_path.to_str().unwrap());
            self.failure_time = Some(Instant::now());
            return Err(io::Error::from(io::ErrorKind::NotFound));
        }

        let _ = self.rotate();

        let now = Utc::now();
        let log_path = self.gen_log_path(now);
        let log_dir = log_path.parent().unwrap();
        if ! log_dir.is_dir() {
            println!("{}: Creating dir '{}'", &self.name, log_dir.to_str().unwrap());
            if let Err(e) = fs::create_dir_all(log_dir) {
                self.failure_time = Some(Instant::now());
                return Err(e);
            }
        }

        let file = match File::create(&log_path) {
            Ok(f) => f,
            Err(e) => {
                self.failure_time = Some(Instant::now());
                return Err(e);
            },
        };
        self.cur_log_path = Some(log_path);
        self.writer = Some(BufWriter::new(file));
        self.start_time = Some(now);
        self.failure_time = None;
        println!("{}: Opened log file: {}", &self.name, self.cur_log_path.as_ref().unwrap().to_str().unwrap());
        Ok(())
    }

    fn close_writer(&mut self, unmount: bool) {
        if let Some(w) = &mut self.writer {
            let _ = w.flush();
            self.writer = None;
            self.start_time = None;
            if let Some(p) = &self.cur_log_path {
                println!("{}: Closed log file: {}", &self.name, p.to_str().unwrap());
                self.cur_log_path = None;
            }
        }
        if unmount {
            let _ = self.unmount();
        }
    }

    fn gen_log_path(&self, start: DateTime<Utc>) -> PathBuf {
        let mut log_path = self.base_path.clone();
        log_path.push(format!("{:04}", start.year()));
        log_path.push(format!("{:02}", start.month()));
        log_path.push(format!("{:02}", start.day()));
        log_path.push(format!("{:02}_{:02}_{:02}.nmea", start.hour(), start.minute(), start.second()));
        log_path
    }

    fn rotate(&self) -> io::Result<()> {
        self.remove_excess_log_files()?;
        self.remove_empty_log_dirs()?;
        Ok(())
    }

    fn remove_excess_log_files(&self) -> io::Result<()> {
        let files = self.sorted_log_files()?;
        let total_size = total_file_size(files.iter())?;
        let free_space = get_free_space(&self.mount_path)?;

        let low_space = if free_space < MIN_LOG_FREE_SPACE { MIN_LOG_FREE_SPACE - free_space } else { 0 };
        let over_size = if total_size > MAX_LOG_DIR_SIZE { total_size - MAX_LOG_DIR_SIZE } else { 0 };
        let max_remove= if total_size > MIN_LOG_DIR_SIZE { total_size - MIN_LOG_DIR_SIZE } else { 0 };
        let to_remove = cmp::max(low_space, over_size);
        let to_remove = cmp::min(to_remove, max_remove);

        if to_remove == 0 {
            return Ok(());
        }

        println!("{}: Total size = {} / {}", &self.name, total_size, MAX_LOG_DIR_SIZE);
        println!("{}: Free space = {} / {}", &self.name, free_space, MIN_LOG_FREE_SPACE);
        println!("{}: Will remove {} bytes", &self.name, to_remove);

        let mut total_bytes_removed = 0;
        let mut total_files_removed = 0;
        for f in files {
            if total_bytes_removed >= to_remove {
                break;
            }
            let size = f.symlink_metadata()?.len();
            println!("{}: Removing '{}' ({} bytes)", &self.name, f.to_str().unwrap(), size);
            std::fs::remove_file(f)?;
            total_bytes_removed += size;
            total_files_removed += 1;
        }
        println!("{}: Removed {} files ({} bytes)", &self.name, total_files_removed, total_bytes_removed);
        Ok(())
    }

    fn remove_empty_log_dirs(&self) -> io::Result<()> {
        let dirs = self.log_tree_dirs()?;
        for d in dirs {
            match std::fs::remove_dir(&d) {
                Ok(_) => println!("{}: Removed empty dir: '{}'", &self.name, &d.to_str().unwrap()),
                Err(e) if matches!(e.raw_os_error(), Some(39)) => (),
                Err(e) => println!("{}: Could not remove dir '{}': {}", &self.name, &d.to_str().unwrap(), e),
            }
        }
        Ok(())
    }

    fn sorted_log_files(&self) -> io::Result<Vec<PathBuf>> {
        let dirs = self.log_dirs()?;
        let mut files: Vec<PathBuf> = dirs
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_file())
            .collect();
        files.sort_unstable();
        Ok(files)
    }

    fn sorted_nmea_files(&self) -> io::Result<Vec<PathBuf>> {
        let dirs = self.log_dirs()?;
        let mut files: Vec<PathBuf> = dirs
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_file())
            .filter(|p| p.extension().map_or(false, |e| e == "nmea"))
            .collect();
        files.sort_unstable();
        Ok(files)
    }

    fn log_dirs(&self) -> io::Result<impl Iterator<Item=PathBuf>> {
        Ok(std::fs::read_dir(&self.base_path)?
            .filter_map(|res| res.ok())
            .filter(|dirent| {
                match dirent.file_name().to_str() {
                    Some(s) => {
                        match s.parse::<u16>() {
                            Ok(y) if y >= 1900 && y < 2100 => true,
                            _ => false,
                        }
                    },
                    _ => false,
                }
            })
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir())
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir())
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir()))
    }

    fn log_tree_dirs(&self) -> io::Result<Vec<PathBuf>> {
        let mut year_dirs: Vec<PathBuf> = std::fs::read_dir(&self.base_path)?
            .filter_map(|res| res.ok())
            .filter(|dirent| {
                match dirent.file_name().to_str() {
                    Some(s) => {
                        match s.parse::<u16>() {
                            Ok(y) if y >= 1900 && y < 2100 => true,
                            _ => false,
                        }
                    },
                    _ => false,
                }
            })
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir())
            .collect();

        let mut month_dirs: Vec<PathBuf> = year_dirs.iter()
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir())
            .collect();

        let mut day_dirs: Vec<PathBuf> = month_dirs.iter()
            .flat_map(|p| std::fs::read_dir(&p).unwrap())
            .filter_map(|res| res.ok())
            .map(|dirent| dirent.path())
            .filter(|p| p.is_dir())
            .collect();

        let mut tree_dirs: Vec<PathBuf> = Vec::new();
        tree_dirs.append(&mut day_dirs);
        tree_dirs.append(&mut month_dirs);
        tree_dirs.append(&mut year_dirs);
        Ok(tree_dirs)
    }

    fn try_mount(&self) -> io::Result<()> {
        if is_mountpoint(&self.mount_path)? {
            return Ok(());
        }

        if !self.manage_mount {
            return Err(io::Error::from(io::ErrorKind::Other));
        }

        let dev_meta = fs::metadata(&self.dev_path)?;
        if !dev_meta.file_type().is_block_device() {
            return Err(io::Error::from(io::ErrorKind::Other));
        }

        match Command::new("fsck").arg("-y").arg(&self.dev_path).status() {
            Ok(r)  => println!("{}: Fsck on {}: {}", &self.name, self.dev_path.to_str().unwrap(), r.code().unwrap_or(256)),
            Err(e) => println!("{}: Warning: Could not run pre-mount fsck: {}", &self.name, e),
        }

        let flags = MsFlags::MS_NOSUID
                  | MsFlags::MS_NODEV
                  | MsFlags::MS_NOEXEC
                  | MsFlags::MS_SYNCHRONOUS
                  | MsFlags::MS_RELATIME
                  | MsFlags::MS_SILENT;

        for t in LOG_FS_TYPES {
            match mount(Some(&self.dev_path), &self.mount_path, Some(*t), flags, None::<&Path>) {
                Ok(_) => {
                    println!("{}: Mounted {} as {}", &self.name, self.dev_path.to_str().unwrap(), t);
                    return Ok(());
                },
                Err(_) => (),
            }
        }
        println!("{}: Failed to mount {} ({})", &self.name, self.mount_path.to_str().unwrap(), self.dev_path.to_str().unwrap());
        Err(io::Error::from(io::ErrorKind::Other))
    }

    fn unmount(&self) -> io::Result<()> {
        if !is_mountpoint(&self.mount_path)? {
            return Ok(());
        }

        if !self.manage_mount {
            return Err(io::Error::from(io::ErrorKind::Other));
        }

        match umount(&self.mount_path) {
                Ok(_) => {
                    println!("{}: Unmounted {} ({})", &self.name, &self.mount_path.to_str().unwrap(), &self.dev_path.to_str().unwrap());
                    return Ok(());
                },
                Err(e) => {
                    println!("{}: Failed to unmount {} ({}): {}", &self.name, &self.mount_path.to_str().unwrap(), &self.dev_path.to_str().unwrap(), e);
                },
        }
        Err(io::Error::from(io::ErrorKind::Other))
    }
}

impl io::Write for NMEALog {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if !self.is_enabled {
            return Ok(0);
        }

        // If we've rolled to a new hour, close the existing log file
        if let Some(start_time) = self.start_time {
            if start_time.hour() != Utc::now().hour() {
                //println!("Hour roll; Closing log");
                self.close_writer(false);
            }
        }

        // Make sure a log file is open
        if self.writer.is_none() {
            let _ = self.open_writer();
        }

        if let Some(w) = &mut self.writer {
            match w.write(buf) {
                Ok(s) => Ok(s),
                Err(e) => {
                    println!("{}: Write to '{}' failed: {}", &self.name, self.cur_log_path.as_ref().unwrap_or(&PathBuf::from("")).to_str().unwrap(), e);
                    self.failure_time = Some(Instant::now());
                    self.close_writer(true);
                    Err(e)
                }
            }
        }
        else {
            Ok(0)
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        if let Some(w) = &mut self.writer {
            w.flush()
        }
        else {
            Ok(())
        }
    }
}

fn load_log_conf() -> Option<LogSettingsJson> {
    let conf_path = Path::new(CONF_DIR).join(Path::new(LOG_CONF_FILENAME));
    match load_log_conf_path(&conf_path) {
        Some(c) => {
            println!("Loaded log config from '{}'", conf_path.to_str().unwrap());
            return Some(c);
        },
        None => {
            println!("Could not load log config from '{}'", conf_path.to_str().unwrap());
        },
    }

    let conf_path = Path::new(DEFAULT_CONF_DIR).join(Path::new(LOG_CONF_FILENAME));
    match load_log_conf_path(&conf_path) {
        Some(c) => {
            println!("Loaded log config from '{}'", conf_path.to_str().unwrap());
            return Some(c);
        },
        None => {
            println!("Could not load log config from '{}'", conf_path.to_str().unwrap());
        },
    }

    None
}

fn load_log_conf_path(conf_path: &Path) -> Option<LogSettingsJson> {
    if !conf_path.is_file() {
        return None;
    }

    let conf_file = match File::open(conf_path) {
        Ok(f) => f,
        _ => return None,
    };

    let conf: LogSettingsJson = match serde_json::from_reader(BufReader::new(conf_file)) {
        Ok(c) => c,
        _ => return None,
    };

    Some(conf)
}

fn total_file_size<'a>(files: impl Iterator<Item=&'a PathBuf>) -> io::Result<u64> {
    Ok(files
        .map(|p| p.symlink_metadata().map_or(0, |m| m.len()))
        .sum())
}

fn get_free_space(path: &Path) -> io::Result<u64> {
    let stat = match statvfs::statvfs(path) {
        Ok(s) => s,
        Err(e) => {
            println!("Error getting free-space at '{}': {}", path.to_str().unwrap(), e);
            return Err(io::Error::from(e));
        }
    };
    Ok(stat.block_size() as u64 * stat.blocks_free() as u64)
}

pub fn is_mountpoint(path: &Path) -> io::Result<bool> {
    let f = File::open("/proc/mounts")?;
    let f = BufReader::new(f);

    Ok(f.lines().any( |line| {
        let line = line.unwrap();
        let dir = line.split(" ").nth(1).unwrap();
        Path::new(dir) == path
    }))
}
